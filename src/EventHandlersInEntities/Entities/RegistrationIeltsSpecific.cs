﻿using System;

namespace EventHandlersInEntities.Entities
{
    public class RegistrationIeltsSpecific : IEntity
    {
        public int Id { get; private set; }
        public Registration Registration { get; private set; }
        public RegistrationStatus RegistrationStatus { get; private set; }

        // For existing entities in DB
        private RegistrationIeltsSpecific()
        {
            SubscribeRegistrationEvents();
        }

        // For new entities
        private RegistrationIeltsSpecific(Registration registration)
        {
            Registration = registration;
            RegistrationStatus = RegistrationStatus.Incomplete;

            SubscribeRegistrationEvents();
        }

        public static RegistrationIeltsSpecific Create(Registration registration)
        {
            return new RegistrationIeltsSpecific(registration);
        }

        private void SubscribeRegistrationEvents()
        {
            Registration.RegistrationCancelled += OnRegistrationCancelled;
        }

        // Quite handy
        private void OnRegistrationCancelled(object registration, EventArgs args) => RegistrationStatus = RegistrationStatus.Cancelled;
    }
}
