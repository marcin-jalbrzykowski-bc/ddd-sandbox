﻿using System;

namespace EventHandlersInEntities.Entities
{
    public class Registration : IAggregateRoot
    {
        public int Id { get; private set; }

        public RegistrationIeltsSpecific RegistrationIeltsSpecific { get; private set; }

        public bool Paid { get; private set; }
        public bool Cancelled { get; private set; }

        private Registration()
        {
            RegistrationIeltsSpecific = RegistrationIeltsSpecific.Create(this); // passing Registration is bad but IeltsSpecific needs 
            Paid = false;                                                       // Registration to subscribe events

            SubscribeRegistrationEvents();
        }

        public static Registration Create()
        {
            // input arguments validation

            // validation passed, we can create Registration
            return new Registration();
        }

        public void Cancel()
        {
            // State machine check if can cancel

            // State machine triggers RegistrationCancelled event
            RegistrationCancelled?.Invoke(this, new EventArgs());
        }

        private void SubscribeRegistrationEvents()
        {
            RegistrationCancelled += OnRegistrationCancelled;
            RegistrationPaid += OnRegistrationPaid;
        }

        private void OnRegistrationCancelled(object registration, EventArgs args) => Cancelled = true;
        private void OnRegistrationPaid(object registration, EventArgs args) => Paid = true;

        public event EventHandler<EventArgs> RegistrationCancelled;
        public event EventHandler<EventArgs> RegistrationPaid;
    }
}
