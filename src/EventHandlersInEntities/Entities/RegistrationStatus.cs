﻿namespace EventHandlersInEntities.Entities
{
    public enum RegistrationStatus
    {
        Incomplete = 1,
        Confirmed = 2,
        Cancelled = 3,
        AwaitingResults = 4,
        HasResults = 5
    }
}
