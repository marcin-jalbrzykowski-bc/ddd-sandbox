﻿using EventHandlersInEntities.Entities;
using Microsoft.EntityFrameworkCore;

namespace EventHandlersInEntities
{
    class SampleDbContext : DbContext
    {
        public DbSet<Registration> Registrations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.;Initial Catalog=EventHandlersInEntities;Integrated Security=true;");
            }

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Registration>()
                .HasOne<RegistrationIeltsSpecific>(x => x.RegistrationIeltsSpecific)
                .WithOne(x => x.Registration)
                .HasPrincipalKey<Registration>(x => x.Id);

            base.OnModelCreating(modelBuilder);
        }
    }
}
