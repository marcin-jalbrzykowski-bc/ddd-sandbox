using System.Linq;
using EventHandlersInEntities.Entities;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace EventHandlersInEntities
{
    public class Tests
    {
        private SampleDbContext _dbContext;

        [SetUp]
        public void Setup()
        {
            _dbContext = new SampleDbContext();
            _dbContext.Database.EnsureCreated();
        }

        [TearDown]
        public void TearDown()
        {
            _dbContext.Dispose();
        }

        [Test]
        [Order(1)]
        public void CreateRegistrationAndSave()
        {
            var registration = Registration.Create();

            _dbContext.Add(registration);
            _dbContext.SaveChanges();

            Assert.That(registration.Cancelled, Is.False);
            Assert.That(registration.RegistrationIeltsSpecific.RegistrationStatus, Is.EqualTo(RegistrationStatus.Incomplete));
        }

        [Test]
        [Order(2)]
        public void CreateRegistrationThenCancelAndSave()
        {
            var registration = Registration.Create();

            _dbContext.Add(registration);
            _dbContext.SaveChanges();

            Assert.That(registration.Cancelled, Is.False);
            Assert.That(registration.RegistrationIeltsSpecific.RegistrationStatus, Is.EqualTo(RegistrationStatus.Incomplete));

            registration.Cancel();

            _dbContext.SaveChanges();

            Assert.That(registration.Cancelled, Is.True);
            Assert.That(registration.RegistrationIeltsSpecific.RegistrationStatus, Is.EqualTo(RegistrationStatus.Cancelled));
        }

        [Test]
        [Order(3)]
        public void GetExistingRegistrationAndCancel()
        {
            var registration = _dbContext.Registrations.Include(x => x.RegistrationIeltsSpecific).First(x => !x.Cancelled);

            Assert.That(registration.Cancelled, Is.False);
            Assert.That(registration.RegistrationIeltsSpecific.RegistrationStatus, Is.EqualTo(RegistrationStatus.Incomplete));

            registration.Cancel();

            _dbContext.SaveChanges();

            Assert.That(registration.Cancelled, Is.True);
            Assert.That(registration.RegistrationIeltsSpecific.RegistrationStatus, Is.EqualTo(RegistrationStatus.Cancelled));
        }
    }
}